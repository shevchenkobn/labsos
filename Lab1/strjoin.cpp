
#include "stdafx.h"
#include "strjoin.h"

wchar_t* wcsjoin(const wchar_t** arr, const int arr_len, const wchar_t* joiner)
{
	size_t byte_count = 0;
	int* len_arr = (int*)malloc(sizeof(int)* arr_len);
	for (int i = 0; i < arr_len; i++)
	{
		byte_count += (len_arr[i] = wcslen(arr[i])) * WCHAR_SIZE;
	}
	int joiner_len = 0;
	if (joiner)
	{
		byte_count += (joiner_len = wcslen(joiner)) * WCHAR_SIZE * (arr_len - 1);
	}

	wchar_t* str = (wchar_t*)malloc(byte_count + WCHAR_SIZE);
	int c = 0;
	for (int i = 0; i < arr_len; i++)
	{
		if (joiner && i)
		{
			for (int j = 0; j < joiner_len; j++)
			{
				str[c++] = joiner[j];
			}
		}
		for (int j = 0; j < len_arr[i]; j++)
		{
			str[c++] = arr[i][j];
		}
	}
	str[c++] = L'\0';
	free(len_arr);
	return str;
}

char* strjoin(const char** arr, const int arr_len, const char* joiner)
{
	size_t byte_count = 0;
	int* len_arr = (int*)malloc(sizeof(int)* arr_len);
	for (int i = 0; i < arr_len; i++)
	{
		byte_count += (len_arr[i] = strlen(arr[i])) * CHAR_SIZE;
	}
	int joiner_len = 0;
	if (joiner)
	{
		byte_count += (joiner_len = strlen(joiner)) * CHAR_SIZE * (arr_len - 1);
	}

	char* str = (char*)malloc(byte_count + CHAR_SIZE);
	int c = 0;
	for (int i = 0; i < arr_len; i++)
	{
		if (joiner && i)
		{
			for (int j = 0; j < joiner_len; j++)
			{
				str[c++] = joiner[j];
			}
		}
		for (int j = 0; j < len_arr[i]; j++)
		{
			str[c++] = arr[i][j];
		}
	}
	str[c++] = '\0';
	free(len_arr);
	return str;
}
