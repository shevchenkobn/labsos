#pragma once

#include "stdafx.h"

#define WCHAR_SIZE sizeof(wchar_t)
#define CHAR_SIZE sizeof(char)

#ifdef UNICODE
#define _tstrjoin wcsjoin
#else
#define _tstrjoin strjoin
#endif // UNICODE


wchar_t* wcsjoin(const wchar_t** arr, const int arr_len, const wchar_t* joiner = NULL);
char* strjoin(const char** arr, const int arr_len, const char* joiner = NULL);