#include "stdafx.h"
#include "fcrevert.h"

#define PROMT_BUFFER_SIZE 1024

const unsigned char UTF8_MARKER[] = { 0xEF, 0xBB, 0xBF };
const unsigned char UTF8_NL[] = { '\r', '\n' };
#define UTF8_MARKER_SIZE 3
const unsigned char UTF16BE_MARKER[] = { 0xFE, 0xFF };
const unsigned char UTF16LE_MARKER[] = { 0xFF, 0xFE };
const unsigned char UTF16BE_NL[] = { '\0', '\r', '\0', '\n' };
const unsigned char UTF16LE_NL[] = { '\r', '\0', '\n', '\0' };
#define UTF16_MARKER_SIZE 2
const unsigned char ANSI_NL[] = { '\r', '\n' };

#define UTF_CHAR_SIZE sizeof (wchar_t)
#define ANSI_CHAR_SIZE sizeof (char)


//#define _CRTDBG_MAP_ALLOC  
//#include <stdlib.h>  
//#include <crtdbg.h>

int fcreverse(const TCHAR* filename);
int flines_reverse(const TCHAR* filename, int element_size, unsigned char* stop_seq, int stop_seq_size, int skip = 0);
int match(unsigned char* seq1, unsigned char* seq2, int size);
int get_offset(unsigned char* haystack, int haystack_size, unsigned char* needle, int needle_size);
void swap_words(unsigned char* line, int line_size, int word_size);

int fcreverse(const TCHAR* filename)
{
	//_CrtDumpMemoryLeaks();

	FILE* file = _tfopen(filename, _T("rb"));
	if (!file)
	{
		return errno ? errno : 1;
	}
	unsigned char* buffer = (unsigned char*)malloc(UTF8_MARKER_SIZE);
	size_t read = fread(buffer, sizeof(unsigned char), UTF8_MARKER_SIZE, file);
	if (read < UTF16_MARKER_SIZE)
	{
		free(buffer);
		fclose(file);
		return flines_reverse(filename, ANSI_CHAR_SIZE, (unsigned char*)ANSI_NL, sizeof(ANSI_NL));
	}
	/*else if (match(buffer, (unsigned char*)UTF8_MARKER, UTF8_MARKER_SIZE))
	{
		free(buffer);
		fclose(file);
		return flines_reverse(filename, UTF_CHAR_SIZE, (unsigned char*)UTF8_NL, sizeof(UTF8_NL), UTF8_MARKER_SIZE);
	}*/
	else if (match(buffer, (unsigned char*)UTF16BE_MARKER, UTF16_MARKER_SIZE))
	{
		free(buffer);
		fclose(file);
		return flines_reverse(filename, UTF_CHAR_SIZE, (unsigned char*)UTF16BE_NL, sizeof(UTF16BE_NL), UTF16_MARKER_SIZE);
	}
	else if (match(buffer, (unsigned char*)UTF16LE_MARKER, UTF16_MARKER_SIZE))
	{
		free(buffer);
		fclose(file);
		return flines_reverse(filename, UTF_CHAR_SIZE, (unsigned char*)UTF16LE_NL, sizeof(UTF16LE_NL), UTF16_MARKER_SIZE);
	}
	else
	{
		free(buffer);
		fclose(file);
		return flines_reverse(filename, ANSI_CHAR_SIZE, (unsigned char*)ANSI_NL, sizeof(ANSI_NL));
	}
}

int match(unsigned char* seq1, unsigned char* seq2, int size)
{
	for (int i = 0; i < size; i++)
	{
		if (seq1[i] != seq2[i])
		{
			return 0;
		}
	}
	return 1;
}

int flines_reverse(const TCHAR* filename, int element_size, unsigned char* stop_seq, int stop_seq_size, int skip)
{
	FILE* file = _tfopen(filename, _T("rb+"));
	if (!file)
	{
		return errno ? errno : 1;
	}
	if (fseek(file, skip, SEEK_SET))
	{
		return errno ? errno : 1;
	}
	unsigned char* buffer = (unsigned char*)malloc(PROMT_BUFFER_SIZE);
	unsigned char* unended_line = NULL;
	int linepos = 0, unended_line_size = 0, eof = 0;
	do
	{
		size_t read = fread(buffer, 1, PROMT_BUFFER_SIZE, file);
		if (!unended_line)
		{
			linepos = ftell(file) - read;
			unended_line_size = read;
			unended_line = (unsigned char*)malloc(read);
			memcpy((void*)unended_line, buffer, unended_line_size);
		}
		else
		{
			unended_line = (unsigned char*)realloc(unended_line, unended_line_size + read);
			memcpy((unended_line + unended_line_size), buffer, read);
			unended_line_size += read;
		}
		int stop_seq_offset = get_offset(unended_line, unended_line_size, stop_seq, stop_seq_size);
		int offset = 0, changes = 0;
		if (stop_seq_offset >= 0)
		{
			do {
				swap_words(unended_line + offset, stop_seq_offset, element_size);
				offset += stop_seq_offset + stop_seq_size;
				stop_seq_offset = get_offset(unended_line + offset, unended_line_size - offset, stop_seq, stop_seq_size);
			} while (stop_seq_offset >= 0);
			changes = 1;
		}
		eof = PROMT_BUFFER_SIZE > read;
		if (eof)
		{
			swap_words(unended_line + offset, unended_line_size - offset, element_size);
			changes = 1;
		}
		if (changes) {
			fseek(file, linepos, SEEK_SET);
			fwrite(unended_line, element_size, offset / element_size, file);
			free(unended_line);
			unended_line = NULL;
		}
	}
	while (!eof);
	fclose(file);
	free(buffer);
	return 0;
}

int get_offset(unsigned char* haystack, int haystack_size, unsigned char* needle, int needle_size)
{
	for (int i = 0; i <= haystack_size - needle_size; i++)
	{
		int match = 1;
		for (int j = 0; j < needle_size; j++)
		{
			if (haystack[i + j] != needle[j])
			{
				match = 0;
				break;
			}
		}
		if (match)
		{
			return i;
		}
	}
	return -1;
}

void swap_words(unsigned char* line, int line_size, int word_size)
{
	//size_t i = (size_t)line, j = (size_t)line + line_size - word_size;
	int swap_size = (int)(line_size / word_size / 2) * word_size;
	for (int i = 0, j = i + line_size - word_size; i < swap_size; i += word_size, j -= word_size) {
		unsigned char temp;
		for (int k = 0; k < word_size; k++)
		{
			temp = line[i + k];
			line[i + k] = line[j + k];
			line[j + k] = temp;
		}
	}
}

