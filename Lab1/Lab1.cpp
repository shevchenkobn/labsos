// Lab1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "fcrevert.h"
#include "strjoin.h"
#define nl() _tprintf(_T("\n"));

int wcscomparator(const void* str1, const void* str2);

int _tmain(int argc, TCHAR* argv[])
{
	if (!_tsetlocale(LC_CTYPE, _T("Russian"))) {
		return 1;
	};
	if (!argc)
	{
		return 2;
	}

	////check what character set is used
	//_tprintf(sizeof TCHAR == 1 ? _T("Multi-Byte Character Set") : _T("Unicode Character Set"));
	//nl();

	//// declare array - the best way
	//const TCHAR* my_family[] = { _T("Татьяна"), _T("Юлиана"), _T("Николай"), _T("Богдан") };
	//const int family_len = sizeof(my_family) / sizeof(TCHAR*);
	//// output array
	//TCHAR* family_string = _tstrjoin(my_family, family_len, (TCHAR*)_T(" "));
	//_tprintf(_T("%s"), family_string);
	//nl();
	//MessageBox(0, family_string, _T("My Family"), MB_OK);
	//free(family_string);

	//// check if unicode
	//bool is = IsTextUnicode((void *)my_family[0], _tcslen(my_family[0]), 0);
	//_tprintf(_T("First TCHAR string is unicode: %s"), is ? _T("true") : _T("false"));
	//nl();

	//// multi-byte array
	//const char* multibyte_family[] = { "Татьяна", "Юлиана", "Николай", "Богдан" };
	//char* multibyte_string = strjoin(multibyte_family, family_len, " ");
	//printf("Multibyte string: %s", multibyte_string);
	//nl();
	//MessageBoxA(0, multibyte_string, "multi-byte array", MB_OK);
	//free(multibyte_string);

	//// to unicode
	//const wchar_t** wide_family = (const wchar_t**)malloc(family_len * sizeof(wchar_t*));
	//for (int i = 0; i < family_len; i++)
	//{
	//	int buffer_size = MultiByteToWideChar(CP_ACP, 0, multibyte_family[i], -1, NULL, 0);
	//	wchar_t* temp = (wchar_t*)malloc(buffer_size * WCHAR_SIZE);
	//	MultiByteToWideChar(CP_ACP, 0, multibyte_family[i], -1, temp, buffer_size);
	//	wide_family[i] = temp;
	//}
	//// output in unicode
	//wchar_t* wide_string = wcsjoin(wide_family, family_len, L" ");	
	//wprintf(L"Unicode string: %s", wide_string);
	//nl();
	//MessageBoxW(0, wide_string, L"wide-character array", MB_OK);
	//free(wide_string);

	//// sort in unicode
	//qsort((void*)wide_family,
	//	family_len,
	//	sizeof(wchar_t*),
	//	(_CoreCrtNonSecureSearchSortCompareFunction) wcscomparator);
	//// output sorted
	//wide_string = wcsjoin(wide_family, family_len, L" ");
	//wprintf(L"Sorted: %s", wide_string);
	//nl();
	//MessageBoxW(0, wide_string, L"wide-character array", MB_OK);
	//free(wide_string);

	//// to multi-byte
	//const char** new_multybyte_array = (const char**)malloc(family_len * sizeof(char*));
	//for (int i = 0; i < family_len; i++)
	//{
	//	int length = WideCharToMultiByte(CP_ACP, 0, wide_family[i], -1, NULL, 0, 0, 0);
	//	char* temp = (char *)malloc(length * CHAR_SIZE);
	//	WideCharToMultiByte(CP_ACP, 0, wide_family[i], -1, temp, length, 0, 0);
	//	new_multybyte_array[i] = temp;
	//}
	//// output new multi-byte
	//multibyte_string = strjoin(new_multybyte_array, family_len, " ");
	//printf("New Multibyte string: %s", multibyte_string);
	//nl();
	//MessageBoxA(0, multibyte_string, "new wide-character array", MB_OK);
	//free(multibyte_string);

	//// clear memory
	//for (int i = 0; i < family_len; i++)
	//{
	//	free((void *)wide_family[i]);
	//	free((void *)new_multybyte_array[i]);
	//}
	//free(wide_family);
	//free(new_multybyte_array);
	
	// revert chars in file
	return fcreverse(argv[1]);
}

int wcscomparator(const void* str1, const void* str2)
{
	return wcscmp(*(wchar_t**)str1, *(wchar_t**)str2);
}

