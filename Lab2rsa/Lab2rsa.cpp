// Lab2rsa.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "number_helpers.h"
#include "Lab2rsa.h"

#define MAX_E_ORDER 1
#define ULONG_SIZE sizeof(unsigned long long)

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
	void GetRSAKeys(const char capacity, unsigned long long* e, unsigned long long* d, unsigned long long* n)
	{
		char order = (char)get_random(MAX_E_ORDER);
		*e = get_ferma_number(order);

		unsigned long long p, q, phi;
		
		do
		{
			p = get_prime(capacity);
			q = get_prime(capacity);
			*n = p * q;
			phi = (p - 1) * (q - 1);
		} while (!mutually_prime(*e, phi));

		long long almost_d = euclid_ext(phi, *e);
		while (almost_d < 0) {
			almost_d += phi;
		}
		*d = almost_d;
	}

	size_t RSAEncrypt(void* source, size_t* dest, size_t size, unsigned long long e, unsigned long long n)
	{
		size_t new_size = size * ULONG_SIZE;
		unsigned long long* ullDest = (unsigned long long *)malloc(new_size);

		char* cSrc = (char*)source;
		for (size_t i = 0; i < size; i++)
		{
			ullDest[i] = (unsigned long long)pows(cSrc[i], e, n);
		}

		*dest = (size_t)ullDest;
		return new_size;
	}

	size_t RSADecrypt(void* source, size_t* dest, size_t size, unsigned long long d, unsigned long long n)
	{
		unsigned long long* ullSrc = (unsigned long long*)source;
		size_t new_size = size / ULONG_SIZE;
		char* cDest = (char*)malloc(new_size);
		for (size_t i = 0; i < new_size; i++)
		{
			cDest[i] = (char)pows(ullSrc[i], d, n); //1717859169
		}
		*dest = (size_t)cDest;
		return new_size;
	}
#ifdef __cplusplus
}
#endif
