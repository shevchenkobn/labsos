#pragma once
#ifdef LAB2RSA_EXPORTS
#define LAB2RSA_FUNC __declspec(dllexport)
#else
#define LAB2RSA_FUNC __declspec(dllimport)
#endif


#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
	LAB2RSA_FUNC void GetRSAKeys(const char capacity, unsigned long long* e, unsigned long long* d, unsigned long long* n);
	LAB2RSA_FUNC size_t RSAEncrypt(void* source, size_t* dest, size_t size, unsigned long long e, unsigned long long n);
	LAB2RSA_FUNC size_t RSADecrypt(void* source, size_t* dest, size_t size, unsigned long long d, unsigned long long n);
#ifdef __cplusplus
}
#endif