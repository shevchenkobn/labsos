#pragma once

unsigned long long get_prime(const char capacity);
bool mutually_prime(unsigned long long a, unsigned long long b);
unsigned long long get_ferma_number(const char order);
long long euclid_ext(unsigned long long a, unsigned long long b);
int get_random(int limit);
unsigned long long pows(unsigned long long number, unsigned long long pow, unsigned long long modulo);