
#include "stdafx.h"
#include "number_helpers.h"


bool is_prime(unsigned long long number);
long long gcd(long long a, long long b);

bool srand_initialized = 0;
int get_random(int limit) {
	if (!srand_initialized) {
		srand(time(0));
		srand_initialized = 1;
	}
	return rand() % limit;
}

unsigned long long get_prime(const char capacity = 12) {
	const unsigned long long limit = pow(2, capacity);
	unsigned long long number;
	char prime;
	do {
		prime = 0;
		number = get_random(limit);
		while (!prime) {
			number++;
			prime = is_prime(number);
		}
	} while (!prime);
	return number;
}

unsigned long long get_ferma_number(const char order)
{
	unsigned long long e = pow(2, order);
	e = pow(2, e) + 1;
	return e;
}

bool mutually_prime(unsigned long long a, unsigned long long b)
{
	return gcd(a, b) == 1;
}

bool is_prime(const unsigned long long number)
{
	if (number == 2)
		return true;
	if (number % 2 == 0)
		return false;
	for (long long i = 3, limit = (long long)sqrt(number) + 1; i < limit; i += 2) {
		if (number % i == 0)
			return false;
	}
	return true;
}

long long gcd(long long firstNum, long long secondNum) {
	while (firstNum != 0 && secondNum != 0) {
		if (firstNum > secondNum)
			firstNum %= secondNum;
		else
			secondNum %= firstNum;
	}
	return firstNum + secondNum;
}

long long euclid_ext(unsigned long long a, unsigned long long b)
{
	long long x = 0, y = 1, u = 1, v = 0,
		gcd = b, m, n, q, r;
	while (a != 0) {
		q = gcd / a;
		r = gcd % a;
		m = x - u * q;
		n = y - v * q;
		gcd = a;
		a = r;
		x = u;
		y = v;
		u = m;
		v = n;
	}
	return y;
}

unsigned long long pows(unsigned long long number, unsigned long long pow, unsigned long long modulo) {
	unsigned long long result = 1;
	number %= modulo;
	while (pow)
	{
		if (pow & 1)
			result = (result * number) % modulo;
		number = (number * number) % modulo;
		pow >>= 1;
	}
	return result;
}