// Lab2static.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../Lab2rsa/Lab2rsa.h"

#define LINE_SIZE 1024
#define PROMT_BUFFER_SIZE 100 * sizeof(TCHAR)
#define TEXT_BUFFER_SIZE 300 * sizeof(TCHAR)



void PrintMessage(int id, HMODULE desc, TCHAR* buffer, int size) {
	LoadString(desc, id, buffer, size);
	_tprintf(_T("%s"), buffer);
}

int main()
{
	system("chcp 1251");
	_tsetlocale(LC_ALL, _T("Russian"));
	HMODULE langDll = LoadLibrary(_T("LanguageTable"));
	if (langDll != 0) {
		TCHAR* messageBuf = (TCHAR*)malloc(PROMT_BUFFER_SIZE);
		TCHAR* inputTextBuf = (TCHAR*)malloc(TEXT_BUFFER_SIZE);
		int langNum = -1;

		while (langNum < 1 || langNum > 4) {
			_tprintf(_T("Choose a langauge:\n1. English\n2. Ukrainian\n3. Russian\n4. Spanish\n"));

			TCHAR ch[2];
			ch[0] = _fgettc(stdin);
			ch[1] = 0;
			langNum = _ttoi(ch);
			fseek(stdin, 0, SEEK_END);
			if (langNum == EOF) {
				_tprintf(_T("\n"));
				continue;
			}
		}

		HMODULE rsaDll = LoadLibrary(_T("Lab2rsa"));
		if (rsaDll != 0) {
			PrintMessage(20 + langNum, langDll, messageBuf, PROMT_BUFFER_SIZE);
			// Generate keys and print them there
			unsigned long long d, e, n;
			GetRSAKeys(12, &e, &d, &n);
			_tprintf(_T("{d, n} = { %d , %d } ; {e, n} = { %d , %d }\n"), d, n, e, n);
			//

			PrintMessage(langNum, langDll, messageBuf, PROMT_BUFFER_SIZE);
			// Text input
			_fgetts(inputTextBuf, TEXT_BUFFER_SIZE, stdin);
			fseek(stdin, 0, SEEK_END);
			//

			// Put encryption there
			char* encoded_line;
			size_t size = (_tcslen(inputTextBuf) + 1) * sizeof(TCHAR);
			size_t encodedSize = RSAEncrypt(inputTextBuf, (size_t*)&encoded_line, size, e, n);
			//
			PrintMessage(40 + langNum, langDll, messageBuf, PROMT_BUFFER_SIZE);
			// And print encrypted there
			printf("0x");
			for (size_t i = 0; i < encodedSize; i++)
			{
				_tprintf(_T("%x"), encoded_line[i]);
			}
			printf("\n");
			//

			// Put decryption there
			TCHAR* decoded_line;

			RSADecrypt(encoded_line, (size_t*)&decoded_line, encodedSize, d, n);
			//
			PrintMessage(60 + langNum, langDll, messageBuf, PROMT_BUFFER_SIZE);
			// And print result there
			_tprintf(_T("%s\n"), decoded_line);
			//
		}
		else {
			_tprintf(_T("RSA library is absent"));
		}
	}
	else {
		_tprintf(_T("Language library hasn't loaded"));
	}
	system("PAUSE");
}
