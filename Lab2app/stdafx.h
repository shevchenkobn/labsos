#define _CRT_SECURE_NO_WARNINGS
// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <Windows.h>
#include <clocale>

typedef void(*GET_RSA_KEYS)(const char capacity, unsigned long long* e, unsigned long long* d, unsigned long long* n);
typedef size_t (*RSA_ENCRYPT)(void* source, size_t* dest, size_t size, unsigned long long e, unsigned long long n);
typedef size_t(*RSA_DECRYPT)(void* source, size_t* dest, size_t size, unsigned long long d, unsigned long long n);

// TODO: reference additional headers your program requires here
